// ==== NAVIGATION ==== //

;(function() {
	var $ = jQuery;
	var mainMenu = $('header nav ul.main-menu');
	var menuItems = $('header nav ul.main-menu li').not('.logo');
	var mobileMenuContainer = $('header .mobile-menu-container');
	var mobileMenuList = $('header .mobile-menu-container ul');

	menuItems.each(function(){
		var itemClone = $(this).clone();
		mobileMenuList.append(itemClone);
	})

	$(document).on('click', 'header .overlay', function(){
		mobileMenuContainer.addClass('active');
		$('body').addClass('no-scroll');
	});

	$('.mobile-menu-container .close-button').on('click', function(){
			mobileMenuContainer.removeClass('active');
			$('body').removeClass('no-scroll');
	});

	var lastScrollTop = 0;
	$(window).scroll(function(event){
		var st = $(this).scrollTop();
		if (st > lastScrollTop){
			//$('header').addClass('hidden');
			console.log($(window).scrollTop());
			$('header').removeClass('sticky');
			if ($(window).scrollTop() >= $('header').height()) {
				$('header').addClass('hidden');
			}
		} else {
			if ($(window).scrollTop() >= $('header').height()) {
				$('header').addClass('sticky');
			} else {
				$('header').removeClass('hidden');
				$('header').removeClass('sticky');
			}
		}
		lastScrollTop = st;
	});
} )();
