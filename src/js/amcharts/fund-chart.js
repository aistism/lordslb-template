
;(function($){
	$(function(){

	var table1 = null;
	var table2 = null;
	var data = null;
	$.get('GAV_REPORT.xml', function(xml){
		data = $.xml2json(xml);
		table1 = data.Tablix1.Details_Collection.Details;
		table2 = data.Tablix2.Details_Collection;
	
		var chart;
		var chartData = [];

		table1.forEach(function(entry) {
			entry.DATA = entry.DATA.split('T')[0];
		    chartData.push(entry);
		});

		// AmCharts.ready(function() {
		// 	makeChart(chartData, 'DATA', '', 'KAINA');
		// });
	});

	});
}(jQuery));

function makeChart(data, chartCategory, valueField1, valueField2) {

    // SERIAL CHART
    chart = new AmCharts.AmStockChart();
    chart.pathToImages = "wp-content/themes/lordslb/img/amcharts/";

    // chart.dataProvider = data;
    // chart.categoryField = chartCategory;
    // chart.startDuration = 1;
    // chart.backgroundColor = "#ffffff";
    // chart.fontFamily = "Playfair Display SC";

	var dataSet = new AmCharts.DataSet();
    dataSet.dataProvider = data;
    dataSet.fieldMappings = [{fromField:valueField2, toField:valueField2}, {fromField:chartCategory, toField:chartCategory}];   
    dataSet.categoryField = chartCategory;          
    chart.dataSets = [dataSet];

    var stockPanel = new AmCharts.StockPanel();
    chart.panels = [stockPanel];              

    var panelsSettings = new AmCharts.PanelsSettings();
    panelsSettings.startDuration = 0;
    chart.panelsSettings = panelsSettings;   

    var graph = new AmCharts.StockGraph();
    graph.valueField = valueField2;
    graph.id = "graph1";
    graph.type = "line";
    graph.lineAlpha = 1;
    graph.lineColor = "#c69c6d";
    graph.lineThickness = 2;
    graph.fillAlphas = 0;
    stockPanel.addStockGraph(graph);
	
	var categoryAxis  = new AmCharts.CategoryAxis();
	categoryAxis.axisColor = "#dadada";
    categoryAxis.axisThickness = 5;
    categoryAxis.axisHeight = 10;
    categoryAxis.axisColor = "#c69c6d";
    categoryAxis.axisAlpha = 1;
    graph.categoryAxis = categoryAxis;

    var categoryAxesSettings = new AmCharts.CategoryAxesSettings();
    categoryAxesSettings.dashLength = 1;
    categoryAxesSettings.id = "categoryAxis";
    categoryAxesSettings.labelRotation = 0;
    categoryAxesSettings.gridPosition = "start";
   
    categoryAxesSettings.gridAlpha = 0.2;
    categoryAxesSettings.girdColor = "#dadada";
	categoryAxesSettings.fontSize = 13;
	categoryAxesSettings.boldLabels = true;
    categoryAxesSettings.color = "#c69c6d";
    categoryAxesSettings.fontFamily = "Playfair Display SC";
    categoryAxesSettings.parseDates = true;
	categoryAxesSettings.minPeriod = "MM";
	categoryAxesSettings.equalSpacing = false;
	categoryAxesSettings.startOnAxis = true;
    chart.categoryAxesSettings = categoryAxesSettings;

    var valueAxesSettings = new AmCharts.ValueAxesSettings();
    valueAxesSettings.dashLength = 1;
    valueAxesSettings.id = "valueAxis";
	valueAxesSettings.axisAlpha = 1;
	valueAxesSettings.gridAlpha = 0;
	valueAxesSettings.axisColor = "#dadada";
	valueAxesSettings.gridColor = "#dadada";
	valueAxesSettings.fontSize = 13;
	valueAxesSettings.boldLabels = true;
    valueAxesSettings.color = "#a4a4a4";
    valueAxesSettings.fontFamily = "Playfair Display SC";

    chart.valueAxesSettings  = valueAxesSettings;

    var chartScrollbarSettings = new AmCharts.ChartScrollbarSettings();
    chartScrollbarSettings.graph = graph;
    chartScrollbarSettings.graphType = "line";
    chart.chartScrollbarSettings = chartScrollbarSettings;

    var chartCursorSettings = new AmCharts.ChartCursorSettings();
    chartCursorSettings.valueBalloonsEnabled = true;
	chartCursorSettings.oneBalloonOnly = true;
	chartCursorSettings.cursorColor = "#c45d52";
	chartCursorSettings.categoryBalloonEnabled = true;
	chartCursorSettings.categoryBalloonAlpha = 0;
	chartCursorSettings.categoryBalloonColor = "#ffffff";
	chartCursorSettings.graphBulletSize = 1;
	chartCursorSettings.bulletsEnabled = true;
    chart.chartCursorSettings = chartCursorSettings;

	var chartBalloon = chart.balloon;
	chartBalloon.adjustBorderColor = false;
	chartBalloon.cornerRadius = 0.0001;
	chartBalloon.shadowAlpha = 0;
	chartBalloon.color = "#575757";
	chartBalloon.borderThickness = 1;
	chartBalloon.borderColor = "#d07d75";
	chartBalloon.fillAlpha = 0.8;
	chartBalloon.fillColor = "#ffffff";
	chartBalloon.fontSize = 18;
	chartBalloon.boldLabels = true;
    chartBalloon.horizontalPadding = 20;
	chartBalloon.verticalPadding = 10;


    var periodSelector = new AmCharts.PeriodSelector();
    periodSelector.periods = [{period:"DD", count:1, label:"1 day"},
                              {period:"DD", count:5, label:"5 days"},
                              {period:"MM", count:1, label:"1 month"},
                              {period:"YYYY", count:1, label:"1 year"},
                              {period:"YTD", label:"YTD"},
                              {period:"MAX", selected:true, label:"MAX"}];                
    chart.periodSelector = periodSelector;
    // var periodSelector = new AmCharts.PeriodSelector();
    // periodSelector.position = "bottom";
    // periodSelector.periods = [{period:"DD", count:1, label:"1 day"},
    //                           {period:"DD", selected:true, count:5, label:"5 days"},
    //                           {period:"MM", count:1, label:"1 month"},
    //                           {period:"YYYY", count:1, label:"1 year"},
    //                           {period:"YTD", label:"YTD"},
    //                           {period:"MAX", label:"MAX"}];                
    // chart.periodSelector = periodSelector;

    //console.log(new AmCharts.PeriodSelector());
	//CURSOR
	// var chartCursor = new AmCharts.ChartCursor();
	// chartCursor.oneBalloonOnly = true;
	// chartCursor.cursorColor = "#c45d52";
	// chartCursor.categoryBalloonEnabled = true;
	// chartCursor.categoryBalloonAlpha = 0;
	// chartCursor.categoryBalloonColor = "#ffffff";
	// chartCursor.graphBulletSize = 1;
	// chartCursor.bulletsEnabled = true;

 //    chartCursor.markerType = "circle";
 //    chartCursor.bullet = "round";
 //    chartCursor.bulletAlpha = 1;
 //    chartCursor.bulletSize = 8;
 //    chartCursor.bulletBorderAlpha = 1;
 //    chartCursor.bulletBorderColor = "#c45d52";
 //    chartCursor.bulletBorderThickness = 2;
 //    chartCursor.bulletColor = "#ffffff";
	// chart.addChartCursor(chartCursor);

	// //BALLOON
	// var chartBalloon = chart.balloon;
	// chartBalloon.adjustBorderColor = false;
	// chartBalloon.cornerRadius = 0.0001;
	// chartBalloon.shadowAlpha = 0;
	// chartBalloon.color = "#575757";
	// chartBalloon.borderThickness = 1;
	// chartBalloon.borderColor = "#d07d75";
	// chartBalloon.fillAlpha = 0.8;
	// chartBalloon.fillColor = "#ffffff";
	// chartBalloon.fontSize = 18;
	// chartBalloon.boldLabels = true;
 //    chartBalloon.horizontalPadding = 20;
	// chartBalloon.verticalPadding = 10;

    // AXES
    // category
 //    var categoryAxis = chart.categoryAxis;
 //    categoryAxis.id = "categoryAxis";
 //    categoryAxis.labelRotation = 0;
 //    categoryAxis.gridPosition = "start";
 //    categoryAxis.axisColor = "#dadada";
 //    categoryAxis.gridAlpha = 0.2;
 //    categoryAxis.girdColor = "#dadada";
 //    categoryAxis.axisThickness = 2;
	// categoryAxis.fontSize = 13;
	// categoryAxis.boldLabels = true;
 //    categoryAxis.color = "#c69c6d";
 //    categoryAxis.fontFamily = "Playfair Display SC";
 //    categoryAxis.parseDates = true;
	// categoryAxis.minPeriod = "MM";
	// categoryAxis.equalSpacing = false;
	// categoryAxis.startOnAxis = true;

	// // Value
	// var valueAxis = new AmCharts.ValueAxis();
 //    valueAxis.id = "valueAxis";
	// valueAxis.axisAlpha = 1;
	// valueAxis.gridAlpha = 0;
	// valueAxis.axisColor = "#dadada";
	// valueAxis.gridColor = "#dadada";
	// valueAxis.fontSize = 13;
	// valueAxis.boldLabels = true;
 //    valueAxis.color = "#a4a4a4";

	// chart.addValueAxis(valueAxis);


    // GRAPH
 //    var graph1 = new AmCharts.StockGraph();
 //    graph1.id = "graph1";
 //    graph1.valueField = valueField1;
 //    graph1.type = "column";
	// graph1.columnWidth = 1;
 //    graph1.lineAlpha = 0;
 //    graph1.fillAlphas = 0.8;
 //    graph1.fontSize = 13;
 //    graph1.fillColors = "#ededed";


    // var graph2 = new AmCharts.StockGraph();
    // graph2.id = "graph2";
    // graph2.valueField = valueField2;
    // graph2.type = "line";
    // graph2.lineAlpha = 1;
    // graph2.lineColor = "#c69c6d";
    // graph2.lineThickness = 2;
    // graph2.fillAlphas = 0;

    // // chart.addGraph(graph1);
    // stockPanel.addStockGraph(graph2);

    chart.write("chartdiv");
}