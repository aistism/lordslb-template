<?php include('inc/image-header.php'); ?>

<section id="about-fund" class="page-block">
	<h2 class="block-header">About the fund</h2>
	
	<div class="page-nav nav-text">
		<ul>
			<li><a href="">performance</a></li>
			<li><a href="">Fund assets</a></li>
			<li><a href="">important info</a></li>
			<li><a href="">atachments</a></li>
		</ul>
	</div>

	<p>
		Our investment management team consists of professionals having asset and risk management experience. 
		Our deal-sourcing, valuation and property management tools enable us to identify attractive assets, 
		execute acquisition at a suitable price, create value in property management and successfully sell the assets.
	</p>
	

	<figure class="fund-info-table label-text">
		<?php for ($i=0; $i<10; $i++) : ?>
		<div class="table-cell">
			<div class="cell-label">Strategy</div>
			<div class="cell-info">Core</div>
		</div>
		<?php endfor; ?>	
	</figure>
	<div class="clear"></div>

</section>


<section id="fund-stats" class="page-block white">
	<h2 class="block-header">Fund performance</h2>

	<div class="header-info label-text">
		<div class="info-block">
			Payments mln. €
			<div class="bar-logo">
				<div class="block-1 block"></div>
				<div class="block-2 block"></div>
				<div class="block-3 block"></div>
			</div>
		</div>
	</div>

	<div class="header-info label-text">
		<div class="info-block">
			Net Asset Value of one fund unit in €
			<div class="graph-logo">
				
			</div>
		</div>
	</div>

	<div id="chartdiv"></div>

	<h2 class="block-header">January 31, 2015</h2>

	<section class="recent-stats label-text">
		<figure class="block">
			<div class="label">Net Asset Value of Lords LB Baltic Fund I</div>
			<div class="value">7’45 mln. €</div>
		</figure>
		<figure class="block">
			<div class="label">Net Asset Value of one Lords LB Baltic Fund Ifund unit</div>
			<div class="value">1.8324€</div>
		</figure>
		<figure class="block">
			<div class="label">Annual return of the Fund since the start of the Fund.</div>
			<div class="value">+12.88%</div>
		</figure>
	</section>
	<div class="clear"></div>
	
	<section class="fund-table">
		<div class="table-holder">
			<?php include('inc/fund-table.php'); ?>
		</div>

		<section class="table-info label-text">
			<div class="left-side">
				<div class="holder">
					<div class="info-icon italic">i</div>
					<div class="text-block">Net Asset Value <span class="strong">€</span></div>
				</div>
				<div class="holder">
					<div class="info-icon gold">i</div>
					<div class="text-block">Proforma IRR (since the start of the Fund). <span class="strong">€</span></div>
				</div>
			</div>
			<div class="right-side">
				<div class="download-link">
					<a href="">Download full document <div class="download-icon"><div class="arrow"></div></div></a>
					
				</div>
			</div>
			<div class="clear"></div>
		</section>
	</section>
	<div class="clear"></div>

</section>

<section id="fund-assets" class="page-block full-width">
	<h2 class="block-header border-bottom">Assets in this fund</h2>
	
	<div class="full-width assets-holder">
		<div class="asset-block">
			<div class="inner-block">
				<div class="image-holder inactive" style="background-image: url('//projects.comjaunuoliai.lt/lordslb/funds/assets/1.jpg');"></div>
			</div>
			<div class="inner-block info">
				<h4 class="status-text label-text">(Divested)</h4>
				<h3 class="border-bottom thick label-text">Metro Plaza</h3>

				<div class="info-table">
					<div class="info-row">
						<div class="info-cell header-text">Location</div>
						<div class="info-cell label-text location"><a href="">Pramonės 6, Kaunas, Lithuania</a></div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Acquisition date</div>
						<div class="info-cell label-text">2010</div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Disposal date</div>
						<div class="info-cell label-text">2013 September</div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Total area</div>
						<div class="info-cell label-text">9’844 sq m</div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Leasable area</div>
						<div class="info-cell label-text">7’972 sq m</div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Year built</div>
						<div class="info-cell label-text">2008</div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Location</div>
						<div class="info-cell label-text location"><a href="">Pramonės 6, Kaunas, Lithuania</a></div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Acquisition date</div>
						<div class="info-cell label-text">2010</div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Disposal date</div>
						<div class="info-cell label-text">2013 September</div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Total area</div>
						<div class="info-cell label-text">9’844 sq m</div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Leasable area</div>
						<div class="info-cell label-text">7’972 sq m</div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Year built</div>
						<div class="info-cell label-text">2008</div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Location</div>
						<div class="info-cell label-text location"><a href="">Pramonės 6, Kaunas, Lithuania</a></div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Acquisition date</div>
						<div class="info-cell label-text">2010</div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Disposal date</div>
						<div class="info-cell label-text">2013 September</div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Total area</div>
						<div class="info-cell label-text">9’844 sq m</div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Leasable area</div>
						<div class="info-cell label-text">7’972 sq m</div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Year built</div>
						<div class="info-cell label-text">2008</div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
		<div class="asset-block">
			<div class="inner-block">
				<div class="image-holder inactive" style="background-image: url('//projects.comjaunuoliai.lt/lordslb/funds/assets/2.jpg');"></div>
			</div>
			<div class="inner-block info">
				<h4 class="status-text label-text">(Divested)</h4>
				<h3 class="border-bottom thick label-text">Metro Plaza</h3>

				<div class="info-table">
					<div class="info-row">
						<div class="info-cell header-text">Location</div>
						<div class="info-cell label-text location"><a href="">Pramonės 6, Kaunas, Lithuania</a></div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Acquisition date</div>
						<div class="info-cell label-text">2010</div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Disposal date</div>
						<div class="info-cell label-text">2013 September</div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Total area</div>
						<div class="info-cell label-text">9’844 sq m</div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Leasable area</div>
						<div class="info-cell label-text">7’972 sq m</div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Year built</div>
						<div class="info-cell label-text">2008</div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="asset-block">
			<div class="inner-block">
				<div class="image-holder inactive" style="background-image: url('//projects.comjaunuoliai.lt/lordslb/funds/assets/3.jpg');"></div>
			</div>
			<div class="inner-block info">
				<h4 class="status-text label-text">(Divested)</h4>
				<h3 class="border-bottom thick label-text">Metro Plaza</h3>

				<div class="info-table">
					<div class="info-row">
						<div class="info-cell header-text">Location</div>
						<div class="info-cell label-text location"><a href="">Pramonės 6, Kaunas, Lithuania</a></div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Acquisition date</div>
						<div class="info-cell label-text">2010</div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Disposal date</div>
						<div class="info-cell label-text">2013 September</div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Total area</div>
						<div class="info-cell label-text">9’844 sq m</div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Leasable area</div>
						<div class="info-cell label-text">7’972 sq m</div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Year built</div>
						<div class="info-cell label-text">2008</div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="asset-block">
			<div class="inner-block">
				<div class="image-holder" style="background-image: url('//projects.comjaunuoliai.lt/lordslb/funds/assets/4.jpg');"></div>
			</div>
			<div class="inner-block info">
				<h3 class="border-bottom label-text">Metro Plaza</h3>

				<div class="info-table">
					<div class="info-row">
						<div class="info-cell header-text">Location</div>
						<div class="info-cell label-text location"><a href="">Pramonės 6, Kaunas, Lithuania</a></div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Acquisition date</div>
						<div class="info-cell label-text">2010</div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Disposal date</div>
						<div class="info-cell label-text">2013 September</div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Total area</div>
						<div class="info-cell label-text">9’844 sq m</div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Leasable area</div>
						<div class="info-cell label-text">7’972 sq m</div>
					</div>
					<div class="info-row">
						<div class="info-cell header-text">Year built</div>
						<div class="info-cell label-text">2008</div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>

	</div>
	<div class="clear"></div>
</section>
<div class="clear"></div>

<section class="page-block text-block">
	<h2 class="block-header border-bottom">Important information</h2>
	<h3 class="header-text center">Dear investor</h3>
	
	<section class="content">
		<p>
			Closed-end real estate investment fund Lords LB Baltic Fund I 
			(hereinafter – the Fund) was launched June 26th, 2009 and has 
			become one of the most active investors in the Baltic countries 
			in commercial real estate projects.
		</p>

		<p>
			During the operations period the Fund issued investment units in 
			the amount of EUR 19'936'487,27. These proceeds were invested into 
			four real estate projects:
		</p>

		<ul>
			<li>Danske Bank office building in Vilnius (acquired May 31st, 2010)</li>
			<li>Metro Plaza office building in Tallinn (acquired October 11th, 2010)</li>
			<li>Prisma retail building in  Riga (acquired November 2nd, 2011)</li>
			<li>Prisma retail building in Kaunas (acquired February 25th, 2012)</li>
		</ul>

		<p>
			As of September 30th, 2014 two assets were  divested:
		</p>

		<ul>
			<li>Danske Bank office building in Vilnius (sold September 30th, 2013)</li>
			<li>Prisma retail building in Riga (sold July 29th, 2014)</li>
		</ul>

		<p>
			As of October 3rd, 2014, total pay-outs to the Fund Investors amount to 
			EUR 12'000'000,07. Net Asset Value of the Fund on the 30th of September 
			2014 was EUR 16'401'175,54. At the moment active negotiations regarding 
			the sale of Metro Plaza office building are undergoing. We expect that 
			this transaction will be closed and the proceeds will be paid-out to the 
			Fund Investors by the end of this year. We are also actively pursuing the 
			divestment process of Prisma retail building in Kaunas. At the moment we 
			are waiting for the official bid proposals. The divestment of this property 
			is expected to happen in Q2 / Q3 2015.
		</p>

		<p>
			The rules and the prospectus of the Fund provide that the Fund will be operating 
			until October 24th, 2014. The term of Fund's operations may be extended for the 
			additional period of 2 years. The decision on the extension of the term of Fund’s 
			operations shall be taken by the Management Board of the Management Company having 
			obtained a written consent of all the Investors of the Fund.
		</p>

		<p>
			The Management Company’s assessment is that the term of the Fund’s operations shall 
			be extended by one year, i.e. until October 24th, 2015. The extension of the term of 
			the Fund’s operations would allow to sell Fund’s investment objects for the price that 
			would be the most favourable to the Fund’s Investors.
		</p>

		<p>
			As the Management Company assesses that the term of Fund’s operations shall be extended, 
			the Management Company initiates the procedure for obtaining the written consents from 
			the Investors. Consequently, we send this notice in order to obtain your written consent 
			for the extension of the term of Fund’s operations until October 24th, 2015. 
			If the Management Company obtains a required number of Investors’ written consents, 
			the Management Board of Management Company will consider the decision to extend the 
			term of Fund’s operations in accordance with terms and conditions set forth in the rules 
			and the prospectus of the Fund.
		</p>

		<p>
			We kindly ask you to provide us with your written consent for the extension of the term 
			of Fund’s operations until October 22nd, 2014. Please fill in the attached form of the 
			consent, sign it and send the signed consent to the Management Company via:
		</p>

		<p>E-mail: <a href="mailto:vt@lordslb.lt" alt="vt@lordslb.lt">vt@lordslb.lt</a> or</p>
		<p>Registered mail:</p>
		<p>Lords LB Asset Management, UAB</p>
		<p>Jogailos str. 4, 01116 Vilnius</p>
		<p>
			If we do not receive your signed consent by October 22nd, 2014, we would consider that 
			you have not agreed with the extension of the term Fund‘s operations until October 24th, 2015.
		</p>
		<p>
			If you have any queries in relation to the proposed extension, please do not hesitate to 
			contact the Management Company.
		</p>
	</section>

	<section class="download-block">
		<h3 class="header-text center">Download atachments</h3>

		<div class="link-block">
			<div class="download-link">
				<a href="">Fund Regulations<div class="download-icon grey"><div class="arrow"></div></div></a>
			</div>
			<br />
			<div class="download-link">
				<a href="">Fund Prospectus<div class="download-icon grey"><div class="arrow"></div></div></a>
			</div>
		</div>
	</section>
</section>
<div class="clear"></div>

<section id="contact-block" class="page-block dark">
	<h2 class="block-header border-bottom gold">Contacts</h2>

	<figure><span class="strong label-text">Phone No.:</span><a class="header-text" href="tel:+370 5 261 94 70">+370 5 261 94 70</a></figure>
	<figure><span class="strong label-text">E-mail:</span><a class="header-text" href="mailto:info@lordslb.lt">info@lordslb.lt</a></figure>
	
	<a href="" class="linked-icon"></a>
</section>