<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><?php wp_title( '-', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<!-- Google Fonts -->
<link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Playfair+Display+SC:400,700' rel='stylesheet' type='text/css'>

<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<header>
    <nav>
        <div class="mobile-menu">Menu</div>
        <ul class="main-menu">
            <li><a href="">About</a></li>
            <li><a href="">Funds</a></li>
            <li><a href="">Investors</a></li>
            <li class="logo"><a href=""></a></li>
            <li><a href="">News</a></li>
            <li><a href="">Our Team</a></li>
            <li><a href="">Contacts</a></li>
        </ul>
    </nav>
    <div class="mobile-menu-container">
        <div class="close-button"></div>
        <div class="mobile-menu-holder">
            <ul></ul>
        </div>
    </div>
    <div class="clear"></div>

    <div class="language">
        <a href="">EN</a>
    </div>
    <div class="overlay"></div>
</header>
